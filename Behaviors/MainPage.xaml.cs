﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Behaviors
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void Submit_Clicked(object sender, System.EventArgs e)
        {
            if(emailEntry.Text == "")
            {
                emailEntry.Placeholder = "Required field";
                emailEntry.PlaceholderColor = Color.Red;
            }
            else
            {
                emailEntry.Placeholder = "";
                emailEntry.PlaceholderColor = Color.Black;
            }
            if (passwordEntry.Text == "")
            {
                passwordEntry.Placeholder = "Required field";
                passwordEntry.PlaceholderColor = Color.Red;
            }
            else
            {
                passwordEntry.Placeholder = "";
                passwordEntry.PlaceholderColor = Color.Black;
            }
            if (confirmPasswordEntry.Text == "")
            {
                confirmPasswordEntry.Placeholder = "Required field";
                confirmPasswordEntry.PlaceholderColor = Color.Red;
            }
            else
            {
                confirmPasswordEntry.Placeholder = "";
                confirmPasswordEntry.PlaceholderColor = Color.Black;
            }
        }
    }
}
