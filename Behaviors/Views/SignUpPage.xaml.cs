﻿using System;
using System.Collections.Generic;
using Behaviors.Utilities.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Behaviors.Views
{
    public partial class SignUpPage : ContentPage
    {
        bool passCheck, emailCheck;


        public SignUpPage()
        {
            InitializeComponent();

        }
        public void Submit_Clicked(object sender, System.EventArgs e)
        {
            if (emailEntry.Text == "")
            {
                EmailLabel.IsVisible = true;
                EmailLabel.TranslateTo(0, -16, 250);
                emailEntry.Placeholder = "Required field";
                emailEntry.PlaceholderColor = Color.Red;
            }
            else
            {
                emailEntry.Placeholder = "";
            }

            if (passwordEntry.Text == "")
            {
                PasswordLabel.IsVisible = true;
                PasswordLabel.TranslateTo(0, -16, 250);
                passwordEntry.Placeholder = "Required field";
                passwordEntry.PlaceholderColor = Color.Red;
            }
            else
            {
                passwordEntry.Placeholder = "";
            }

            if (confirmPasswordEntry.Text == "")
            {
                ConfirmPasswordLabel.IsVisible = true;
                ConfirmPasswordLabel.TranslateTo(0, -18, 250);
                confirmPasswordEntry.Placeholder = "Required field";
                confirmPasswordEntry.PlaceholderColor = Color.Red;
            }
            else
            {
                confirmPasswordEntry.Placeholder = "";
            }

            if(confirmPasswordEntry.Text != passwordEntry.Text )
            {
                passwordEntry.Text = "";
                passwordEntry.Placeholder = "Password did not match";
                passwordEntry.PlaceholderColor = Color.Red;
                confirmPasswordEntry.Text = "";
                confirmPasswordEntry.Placeholder = "Password did not match";
                confirmPasswordEntry.PlaceholderColor = Color.Red;
                passCheck = false;
            }
            else
            {
                if(passwordEntry.Text == "" && confirmPasswordEntry.Text == "")
                {
                    passCheck = false;
                }
                else
                {
                    passCheck = true;
                }

            }

            if(EmailCheckHelper.Check(emailEntry.Text))
            {
                checkButton.Text = "✓";
                checkButton.BackgroundColor = Color.FromRgb(85,239,196);
                emailCheck = true;
            }
            else{
                checkButton.Text = "x";
                checkButton.BackgroundColor = System.Drawing.Color.FromArgb(255,118,117);
                emailCheck = false;
            }

            if(emailCheck && passCheck){
                DisplayAlert("Sign-up","Successful","Ok");
            }
        }

        void EmailEntry_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            EmailLabel.IsVisible = true;
            EmailLabel.TranslateTo(0, -16, 250);
            ((Entry)sender).Placeholder = "";
        }

        void EmailEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (((Entry)sender).Text == "")
            {
                EmailLabel.TranslateTo(0, 16, 250);
                EmailLabel.IsVisible = false;
                ((Entry)sender).Placeholder = "Email";
            }
        }

        void PasswordEntry_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            PasswordLabel.IsVisible = true;
            PasswordLabel.TranslateTo(0, -16, 250);
            ((Entry)sender).Placeholder = "";
        }

        void PasswordEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if(((Entry)sender).Text == "")
            {
                PasswordLabel.TranslateTo(0, 16, 250);
                PasswordLabel.IsVisible = false;
                ((Entry)sender).Placeholder = "Password";

            }
        }
        void ConfirmPasswordEntry_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
                ConfirmPasswordLabel.IsVisible = true;
                ConfirmPasswordLabel.TranslateTo(0, -18, 250);
                ((Entry)sender).Placeholder = "";
        }

        void ConfirmPasswordEntry_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            if (((Entry)sender).Text == "")
            {
                ConfirmPasswordLabel.TranslateTo(0, 18, 250);
                ConfirmPasswordLabel.IsVisible = false;
                ((Entry)sender).Placeholder = "Confirm Password";
                ((Entry)sender).PlaceholderColor = Color.Black;
            }
        }

        void Password_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            PasswordScore passwordScore = PasswordStrengthHelper.CheckStrength(passwordEntry.Text);
            switch(passwordScore)
            {
                case PasswordScore.Blank:
                    passwordStrengthLabel.Text = "Blank";
                    passwordStrengthLabel.TextColor = Color.Black;
                    break;
                case PasswordScore.VeryWeak:
                    passwordStrengthLabel.Text = "Very Weak";
                    passwordStrengthLabel.TextColor = Color.FromRgb(214,48,49);
                    break;
                case PasswordScore.Weak:
                    passwordStrengthLabel.Text = "Weak";
                    passwordStrengthLabel.TextColor = Color.FromRgb(255, 118, 117);
                    break;
                case PasswordScore.Medium:
                    passwordStrengthLabel.Text = "Medium";
                    passwordStrengthLabel.TextColor = Color.FromRgb(253, 203, 110);
                    break;
                case PasswordScore.Strong:
                    passwordStrengthLabel.Text = "Strong";
                    passwordStrengthLabel.TextColor = Color.FromRgb(85, 239, 196);
                    break;
                case PasswordScore.VeryStrong:
                    passwordStrengthLabel.Text = "Very Strong";
                    passwordStrengthLabel.TextColor = Color.FromRgb(0, 184, 148);
                    break;
            }
        }
    }
}
