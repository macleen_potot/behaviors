﻿using System;
using Xamarin.Forms;
namespace Behaviors.Behavior
{
    public class EntryUnfocusedBehavior:Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.Unfocused += OnEntryUnfocused;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.Unfocused -= OnEntryUnfocused;
            base.OnDetachingFrom(entry);
        }

        void OnEntryUnfocused(object sender, EventArgs e)
        {
            ((Entry)sender).PlaceholderColor = Color.Default;
            ((Entry)sender).TextColor = Color.Default;
        }
    }
}
