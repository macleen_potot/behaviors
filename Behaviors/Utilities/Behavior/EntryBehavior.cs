﻿using System;
using Xamarin.Forms;
namespace Behaviors.Behavior
{
    public class EntryBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.Focused += OnEntryFocused;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.Focused -= OnEntryFocused;
            base.OnDetachingFrom(entry);
        }

        void OnEntryFocused(object sender, EventArgs e)
        {
            ((Entry)sender).TextColor = Color.Blue;
        }
    }
}

