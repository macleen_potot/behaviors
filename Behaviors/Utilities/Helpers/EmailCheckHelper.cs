﻿using System;
using System.Text.RegularExpressions;
namespace Behaviors.Utilities.Helpers
{
    public class EmailCheckHelper
    {
        public static bool Check(string value)  
        {  
            if (value == null)  
            {  
                return false;  
            }  

            var str = value as string;  
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,4})+)$");  
            Match match = regex.Match(str);  

            return match.Success;  
        }  
    }
}
